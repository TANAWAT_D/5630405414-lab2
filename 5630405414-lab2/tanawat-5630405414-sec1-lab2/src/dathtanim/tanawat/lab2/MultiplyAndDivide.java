package dathtanim.tanawat.lab2;

public class MultiplyAndDivide {

	public static void main(String[] args) {
		
		String op = args[0];
		double num1 = Double.parseDouble(args[1]); 
		double num2 = Double.parseDouble(args[2]); 		
		double multiply = num1 * num2 ;
		double divide = num1 / num2 ;
		if (op.equals("x")){
			System.out.println(num1 + " " + args[0] + " " + num2 + " = " + multiply);
		}
		else if (op.equals("/")){
			if (args[2].equals("0")){
				 System.out.println("can't divide by zero");
			}
			else System.out.println(num1 + " " + args[0] + " " + num2 + " = " + divide);
		}

	}

}
