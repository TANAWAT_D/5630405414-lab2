package dathtanim.tanawat.lab2;

public class RecComputation {

	public static void main(String[] args) {
		
		double w  = Double.parseDouble(args[0]);
		double h  = Double.parseDouble(args[1]);
		System.out.print("The circumference of a rectangle with width = " + w);
		System.out.print(" and height = " + h);
		System.out.print(" is " + (w + h) * 2 );
		System.out.print(" and its area is " + (w * h));
	}

}
