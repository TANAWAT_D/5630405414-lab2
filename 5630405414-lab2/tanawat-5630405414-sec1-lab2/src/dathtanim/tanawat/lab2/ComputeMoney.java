package dathtanim.tanawat.lab2;

public class ComputeMoney {

	public static void main(String[] args) {

		if (args.length != 4) {
			System.err.println("ComputeMoney <1,000 Baht> <500 Baht> <100 Baht> <20 baht>");

		} 
		else if (args.length == 4) {
			
			double num1000 = Double.parseDouble(args[0]);
			double num500 = Double.parseDouble(args[1]);
			double num100 = Double.parseDouble(args[2]);
			double num20 = Double.parseDouble(args[3]);
			double total1000 = num1000 * 1000;
			double total500 = num500 * 500;
			double total100 = num100 * 100;
			double total20 = num20 * 20;
			double total = total1000 + total500 + total100 + total20;
			System.out.println("Total Money is " + total);
		}

	}

}
