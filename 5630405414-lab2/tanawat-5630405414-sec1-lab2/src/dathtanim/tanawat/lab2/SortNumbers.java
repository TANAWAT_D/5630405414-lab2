package dathtanim.tanawat.lab2;
import java.util.Arrays;
public class SortNumbers {

	public static void main(String[] args) {
		
		 double[] input = new double[args.length];
		 
		    for (int i = 0; i < args.length; i++) {
		        input[i] = Double.parseDouble(args[i]);
		    }
		    Arrays.sort(input);
		    for(int i = 0; i < args.length; i++){
		    	System.out.print(input[i] + " ");
		    }			    

	}

}
