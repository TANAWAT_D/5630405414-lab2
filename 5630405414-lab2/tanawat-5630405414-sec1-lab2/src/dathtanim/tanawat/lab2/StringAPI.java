package dathtanim.tanawat.lab2;

public class StringAPI {

	public static void main(String[] args) {
		String schoolName = args[0];
		String[] name = schoolName.split(" ");
		String university = name[name.length-1].toLowerCase();
		String college = name[name.length-1].toLowerCase();	
		
		if(schoolName.toLowerCase().contains("university")){
			System.out.println(schoolName.replace("university","") + " is a " + university);
		}		
		else if(schoolName.toLowerCase().contains("college")){
			System.out.println(schoolName.replace("college","") + " is a " + college);
		}
		else 
			System.out.println(schoolName + " is neither university nor college.");
	}

}
