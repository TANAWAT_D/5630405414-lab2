package dathtanim.tanawat.lab2;

class Footballer {
	
		public static void main(String[] args) {
			if (args.length != 3){
				System.err.println("Footballer <footballer name> <nationality> <club name>");
				System.exit(1);
			}
			System.out.println("My Favourite football player is " + args[0]);
			System.out.println("His national is " + args[1] + 
								" He plays for " + args[2]);
			
		}
}