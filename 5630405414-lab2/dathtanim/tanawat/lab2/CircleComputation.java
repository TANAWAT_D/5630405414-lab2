package dathtanim.tanawat.lab2;
class CircleComputation {
		public static void main(String[] args) {
			double r  = Double.parseDouble(args[0]);
			System.out.print("Circle with radius " + r);
			System.out.print(" has the area as ");
			System.out.print(Math.PI*Math.pow(r, 2));
			System.out.print(" and the circumference as ");
			System.out.print(2*Math.PI*r);
			
		}
}